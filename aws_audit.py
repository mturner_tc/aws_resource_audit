import boto3
from tabulate import tabulate
from pprint import pprint

client = boto3.client('ec2')

response = client.describe_reserved_instances(
    Filters=[{
        'Name':'state',
        'Values': ['active']
    }])


condensed_reservations = []
for res in response['ReservedInstances']:
    InstanceOS = "linux" if "Linux/UNIX" in res['ProductDescription'] else "windows"
    region = boto3.session.Session().region_name
    ScopedRegionAZ = region if res['Scope'] == "Region" else res['AvailabilityZone']
    VPC = "VPC" in res['ProductDescription']
    condensed_reservations.append({'InstanceType':res['InstanceType'], 'Platform': InstanceOS, 'Scope': res['Scope'], 'AZ': ScopedRegionAZ, 'Count': res['InstanceCount'], 'VPC': VPC})

# Print a table
rows =  [[x['InstanceType'], x['Platform'], x['VPC'], x['AZ'], x['Scope'], x['Count']] for x in condensed_reservations]

print "\t\tReservations"
print tabulate(rows, headers=['InstanceType', 'Platform', 'VPC', 'AZ', 'Scope', 'Count'],tablefmt="psql")


region_res = [x for x in condensed_reservations if x['Scope'] == 'Region']
az_res = [x for x in condensed_reservations if x['Scope'] == 'Availability Zone']


resp = client.describe_instances(Filters=[{'Name':'instance-state-name', 'Values': ['running']}])['Reservations']
full_instances = []
for i in resp:
    full_instances.extend(i['Instances'])

condensed_instances = [{'AZ':x['Placement']['AvailabilityZone'], 'InstanceType': x['InstanceType'], 'Platform': 'windows' if 'Platform' in x else 'linux', 'VPC': 'VpcId' in x, 'InstanceId': x['InstanceId']} for x in full_instances]

def check_instance_match_res(inst, res):
    # Is res available
    if res['Count'] > 0:
        # Region match
        if res['AZ'] == inst['AZ'] or res['Scope'] == 'Region':
            # Instance Type match
            if res['InstanceType'] == inst['InstanceType']:
                # VPC match
                if res['VPC'] == inst['VPC']:
                    # Platform match
                    if res['Platform'] == inst['Platform']:
                        return True
    return False

# Handle AZ Specific Reservations first
for idx_az, az in enumerate(az_res):
    for idx_inst, inst in enumerate(condensed_instances):
        if check_instance_match_res(inst, az):
            az['Count'] = az['Count'] - 1
            inst['Covered'] = True

# Handle Region wide reservations
for idx_az, reg in enumerate(region_res):
    for idx_inst, inst in enumerate(condensed_instances):
        if check_instance_match_res(inst, reg):
            reg['Count'] = reg['Count'] - 1
            inst['Covered'] = True

# Tables


print "\t\tRunning Instances"
rows =  [[x['InstanceType'], x['Platform'], x['VPC'], x['AZ'], x['InstanceId']] for x in condensed_instances]
print tabulate(rows, headers=['InstanceType', 'Platform', 'VPC', 'AZ', 'InstanceId'],tablefmt="psql")

print "\t\tUnused Reservations"
rows =  [[x['InstanceType'], x['Platform'], x['VPC'], x['AZ'], x['Scope'], x['Count']] for x in condensed_reservations if x['Count'] > 0]
print tabulate(rows, headers=['InstanceType', 'Platform', 'VPC', 'AZ', 'Scope', 'Count'],tablefmt="psql")

print "\t\tUncovered Instances"
rows =  [[x['InstanceType'], x['Platform'], x['VPC'], x['AZ'], x['InstanceId']] for x in condensed_instances if 'Covered' in x]
print tabulate(rows, headers=['InstanceType', 'Platform', 'VPC', 'AZ', 'InstanceId'],tablefmt="psql")
