import boto3
from pprint import pprint
from collections import Counter

client = boto3.client('ec2')

reservations = client.describe_reserved_instances(Filters= [{'Name' : 'state', 'Values':['active']}])['ReservedInstances']
instances = client.describe_instances(Filters = [{'Name' : 'instance-state-name', 'Values':['running']}])['Reservations']


class TInstance(object):
    covered = False
    """docstring for TInstance"""
    def __init__(self, aws_instance):
        super(TInstance, self).__init__()
        self.az = aws_instance['Placement']['AvailabilityZone']
        self.instance_class = aws_instance['InstanceType']
        self.os = aws_instance['Platform'] if 'Platform' in aws_instance else "linux"
        self.is_vpc = 'VpcId' in aws_instance
        self.name = "no-name"
        self.instance_id = aws_instance['InstanceId']
        for t in aws_instance['Tags']:
            if t['Key'] == 'Name':
                self.name = t['Value'] 
    def __str__(self):
        template = "AZ: {}\nType: {}\nOS: {}\nVPC: {}\nName: {}\nId: {}"
        return template.format(self.az, self.instance_class, self.os, self.is_vpc, self.name, self.instance_id)

class TReservation(object):
    """docstring for TReservation"""
    def __init__(self, aws_reservation):
        super(TReservation, self).__init__()
        if aws_reservation['Scope'] == 'Region':
            aws_reservation['AvailabilityZone'] = "region"
        self.az = aws_reservation['AvailabilityZone']
        self.count = aws_reservation['InstanceCount']
        self.instance_class = aws_reservation['InstanceType']
        self.os = aws_reservation['ProductDescription']
        self.is_vpc = "VPC" in self.os
        self.id = aws_reservation['ReservedInstancesId']
    # def __str__(self):

tc_reservations = [TReservation(x) for x in reservations]
aws_instances = []
for i in instances:
    aws_instances.extend(i['Instances'])
tc_instances = [TInstance(x) for x in aws_instances]





for tdx, ti in enumerate(tc_instances):
    # iterate reservations and find a match.
    for idx, res in enumerate(tc_reservations):
        if res.count > 0 and ti.is_vpc == res.is_vpc and ti.instance_class == res.instance_class and ti.os in res.os.lower() and (ti.az == res.az or res.az == "region"):
            nr = res
            nr.count = nr.count - 1
            tc_reservations[idx] = nr
            ti.covered = True
            continue

covered_instances = [x for x in tc_instances if x.covered == True]
not_covered_instances = [x for x in tc_instances if x.covered == False]
not_used_reservations = [x for x in tc_reservations if x.count > 0]

template = '''
Covered Instances: {}
Not Covered Instances: {}
'''

print template.format(len(covered_instances), len(not_covered_instances))

for nur in not_used_reservations:
    print "{} in {} of class {} is not used. Overage Qty: {}".format(nur.id, nur.az, nur.instance_class, nur.count)

lot = [(x.instance_class, x.az) for x in not_covered_instances]

count = Counter(lot)
for i in count:
    out = "Buy {} {} in {}".format(count[i], i[0], i[1])
    print(out)
